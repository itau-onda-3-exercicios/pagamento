package br.com.itau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.converter.RecordMessageConverter;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;
import org.springframework.messaging.handler.annotation.Payload;

import br.com.itau.clients.PedidoClient;
import br.com.itau.pagamento.viewobjects.Pagamento;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class App 
{
	@Autowired
	PedidoClient pedidoClient;
	
    public static void main( String[] args )
    {
        SpringApplication.run(App.class, args);
    }
    
	@Bean
	public RecordMessageConverter converter() {
		return new StringJsonMessageConverter();
	}
    
	@KafkaListener(id="pedidos", topics="PagamentosProcessados")
	public void consumir(@Payload Pagamento pagamento) {
		pedidoClient.atualizarPagamento(pagamento);		
	}
	
}
