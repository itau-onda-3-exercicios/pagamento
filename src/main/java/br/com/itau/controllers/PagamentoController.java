package br.com.itau.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimentoproduto.services.PagamentoService;
import br.com.itau.pagamento.viewobjects.Pagamento;

@RestController
@RequestMapping
public class PagamentoController {
	@Autowired
	PagamentoService pagamentoService;

	@GetMapping("/{id}")
	public ResponseEntity buscar(@PathVariable Integer id){
		Pagamento pagamento = pagamentoService.consultar(id);
		return ResponseEntity.ok(pagamento);
	}
	
	@PostMapping("/{id}")
	public ResponseEntity solicitarPagamento(@PathVariable Integer id){
		Pagamento pagamento = pagamentoService.solicitar(id);		
		return ResponseEntity.status(201).body(pagamento);
	}
}
