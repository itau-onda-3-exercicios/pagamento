package br.com.itau.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.itau.pagamento.viewobjects.Pagamento;

@FeignClient(name="pedido")
public interface PedidoClient {

	@PostMapping("/pedido/atualizar")
	public ResponseEntity atualizarPagamento(@RequestBody Pagamento pagamento);
}
