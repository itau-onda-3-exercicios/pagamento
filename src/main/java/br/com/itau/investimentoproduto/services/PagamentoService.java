package br.com.itau.investimentoproduto.services;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import br.com.itau.pagamento.viewobjects.Pagamento;

@Service
public class PagamentoService {

	@Autowired
	KafkaTemplate<Object, Object> kafkaTemplate;
	
	public Pagamento consultar(Integer id) {
		Pagamento pagamento = new Pagamento();

		//ler da fila
		
		return pagamento;
	}
	
	public Pagamento solicitar(Integer id) {
		Pagamento pagamento = new Pagamento();
		
		pagamento.setId(id);
		kafkaTemplate.send("PagamentosPendentes", pagamento);
		
		return pagamento;
	}
	
	private boolean getRandomBoolean() {
	    Random random = new Random();
	    return random.nextBoolean();
	}	
}
